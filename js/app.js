var Text = angular.module('Text', []);

Text.controller('RibbonController', function($scope) {
	'use strict';

	$scope.showHomeTab = true;
	$scope.showInsertTab = false;
	$scope.showViewTab = false;

	$scope.showTab = function(tab) {
		switch(tab) {
			case 'home':
				$scope.showInsertTab = false;
				$scope.showViewTab = false;
				$scope.showHomeTab = true;
				break;

			case 'insert':
				$scope.showHomeTab = false;
				$scope.showViewTab = false;
				$scope.showInsertTab = true;
				break;

			case 'view':
				$scope.showHomeTab = false;
				$scope.showInsertTab = false;
				$scope.showViewTab = true;
				break;
		}
	};

	$scope.showFileMenu = false;

	$scope.openFileMenu = function() {
		$scope.showFileMenu = true;
	};
});